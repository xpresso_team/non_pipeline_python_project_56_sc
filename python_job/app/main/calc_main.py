# Main Class
from python_job.app.python_calc import Calculator


class Main:
    """
    Main Class
    """
    if __name__ == "__main__":
        calc = Calculator(3, 6)
        print(f'Addition:{calc.add()}')
